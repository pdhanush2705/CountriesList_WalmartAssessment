Possible extensions that could be added if the project scope increases:
   *  Architecture update: Could have more modularity with dependecy injection using dagger or koin and seperate repository to inject  service. ViewModel.Factory can be used to intialize ViewModel with parameters to its constructors.
   * Can use OkHttpClient for retry mechanisms on connection timeouts.

Output screenshot have also been added to the directory.
